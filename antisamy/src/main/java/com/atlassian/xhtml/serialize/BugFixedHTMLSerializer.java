package com.atlassian.xhtml.serialize;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

import org.apache.xerces.dom.DOMMessageFormatter;
import org.apache.xerces.util.XMLChar;
import org.apache.xml.serialize.DOMSerializerImpl;
import org.apache.xml.serialize.ElementState;
import org.apache.xml.serialize.Method;
import org.apache.xml.serialize.OutputFormat;
import org.w3c.dom.DOMError;
import org.w3c.dom.ls.LSException;
import org.xml.sax.SAXException;

import org.apache.xml.serialize.HTMLSerializer;

/**
 * <p>
 * The Xerces HTMLSerializer has bugs where it has mixed up the character 0xF7 with 0X7F (del).
 * </p>
 * <p>
 * Later versions of Xerces fix this but aren't available in a Maven repo. See
 * https://extranet.atlassian.com/jira/browse/ADM-14197 so in the mean-time this class exists to fix the described bugs.
 * It is heavily copy and pasted from the BaseMarkupSerializer.
 * </p>
 */
public class BugFixedHTMLSerializer extends HTMLSerializer
{
    static short WELLFORMED = 0x1 << 1;
    static short SPLITCDATA = 0x1 << 4;

    /**
     * Constructs a new HTML/XHTML serializer depending on the value of <tt>xhtml</tt>. The serializer cannot be used
     * without calling {@link #setOutputCharStream} or {@link #setOutputByteStream} first.
     * 
     * @param xhtml True if XHTML serializing
     */
    protected BugFixedHTMLSerializer(boolean xhtml, OutputFormat format)
    {
        super(xhtml, format);
    }

    /**
     * Constructs a new serializer. The serializer cannot be used without calling {@link #setOutputCharStream} or
     * {@link #setOutputByteStream} first.
     */
    public BugFixedHTMLSerializer()
    {
        super();
    }

    /**
     * Constructs a new serializer. The serializer cannot be used without calling {@link #setOutputCharStream} or
     * {@link #setOutputByteStream} first.
     */
    public BugFixedHTMLSerializer(OutputFormat format)
    {
        super(format);
    }

    /**
     * Constructs a new serializer that writes to the specified writer using the specified output format. If
     * <tt>format</tt> is null, will use a default output format.
     * 
     * @param writer The writer to use
     * @param format The output format to use, null for the default
     */
    public BugFixedHTMLSerializer(Writer writer, OutputFormat format)
    {
        super(writer, format);
    }

    /**
     * Constructs a new serializer that writes to the specified output stream using the specified output format. If
     * <tt>format</tt> is null, will use a default output format.
     * 
     * @param output The output stream to use
     * @param format The output format to use, null for the default
     */
    public BugFixedHTMLSerializer(OutputStream output, OutputFormat format)
    {
        super(output, format);
    }

    @Override
    protected void printEscaped(int ch) throws IOException
    {
        String charRef;
        // If there is a suitable entity reference for this
        // character, print it. The list of available entity
        // references is almost but not identical between
        // XML and HTML.
        charRef = getEntityRef(ch);
        if (charRef != null)
        {
            _printer.printText('&');
            _printer.printText(charRef);
            _printer.printText(';');
        }
        else if ((ch >= ' ' && _encodingInfo.isPrintable((char) ch) && ch != 0x7F) || ch == '\n' || ch == '\r'
                || ch == '\t')
        {
            // Non printables are below ASCII space but not tab or line
            // terminator, ASCII delete, or above a certain Unicode threshold.
            if (ch < 0x10000)
            {
                _printer.printText((char) ch);
            }
            else
            {
                _printer.printText((char) (((ch - 0x10000) >> 10) + 0xd800));
                _printer.printText((char) (((ch - 0x10000) & 0x3ff) + 0xdc00));
            }
        }
        else
        {
            myPrintHex(ch);
        }
    }

    @Override
    public void characters(char[] chars, int start, int length) throws SAXException
    {
        org.apache.xml.serialize.ElementState state;

        try
        {
            // HTML: no CDATA section
            state = content();
            state.doCData = false;

            // Check if text should be print as CDATA section or unescaped
            // based on elements listed in the output format (the element
            // state) or whether we are inside a CDATA section or entity.

            if (state.inCData || state.doCData)
            {
                int saveIndent;

                // Print a CDATA section. The text is not escaped, but ']]>'
                // appearing in the code must be identified and dealt with.
                // The contents of a text node is considered space preserving.
                if (!state.inCData)
                {
                    _printer.printText("<![CDATA[");
                    state.inCData = true;
                }
                saveIndent = _printer.getNextIndent();
                _printer.setNextIndent(0);
                char ch;
                final int end = start + length;
                for (int index = start; index < end; ++index)
                {
                    ch = chars[index];
                    if (ch == ']' && index + 2 < end && chars[index + 1] == ']' && chars[index + 2] == '>')
                    {
                        _printer.printText("]]]]><![CDATA[>");
                        index += 2;
                        continue;
                    }
                    if (!XMLChar.isValid(ch))
                    {
                        // check if it is surrogate
                        if (++index < end)
                        {
                            surrogates(ch, chars[index], true);
                        }
                        else
                        {
                            fatalError("The character '" + ch + "' is an invalid XML character");
                        }
                        continue;
                    }
                    if ((ch >= ' ' && _encodingInfo.isPrintable(ch) && ch != 0x7F) || ch == '\n' || ch == '\r'
                            || ch == '\t')
                    {
                        _printer.printText(ch);
                    }
                    else
                    {
                        // The character is not printable -- split CDATA section
                        _printer.printText("]]>&#x");
                        _printer.printText(Integer.toHexString(ch));
                        _printer.printText(";<![CDATA[");
                    }
                }
                _printer.setNextIndent(saveIndent);

            }
            else
            {

                int saveIndent;

                if (state.preserveSpace)
                {
                    // If preserving space then hold of indentation so no
                    // excessive spaces are printed at line breaks, escape
                    // the text content without replacing spaces and print
                    // the text breaking only at line breaks.
                    saveIndent = _printer.getNextIndent();
                    _printer.setNextIndent(0);
                    printText(chars, start, length, true, state.unescaped);
                    _printer.setNextIndent(saveIndent);
                }
                else
                {
                    printText(chars, start, length, false, state.unescaped);
                }
            }
        }
        catch (IOException except)
        {
            throw new SAXException(except);
        }
    }

    @Override
    protected void printCDATAText(String text) throws IOException
    {
        int length = text.length();
        char ch;

        for (int index = 0; index < length; ++index)
        {
            ch = text.charAt(index);
            if (ch == ']' && index + 2 < length && text.charAt(index + 1) == ']' && text.charAt(index + 2) == '>')
            { // check for ']]>'
                if (fDOMErrorHandler != null)
                {
                    // REVISIT: this means that if DOM Error handler is not registered we don't report any
                    // fatal errors and might serialize not wellformed document
                    if ((features & SPLITCDATA) == 0)
                    {
                        String msg = DOMMessageFormatter.formatMessage(DOMMessageFormatter.SERIALIZER_DOMAIN,
                                "EndingCDATA", null);
                        if ((features & WELLFORMED) != 0)
                        {
                            // issue fatal error
                            modifyDOMError(msg, DOMError.SEVERITY_FATAL_ERROR, "wf-invalid-character", fCurrentNode);
                            fDOMErrorHandler.handleError(fDOMError);
                            throw new LSException(LSException.SERIALIZE_ERR, msg);
                        }
                        // issue error
                        modifyDOMError(msg, DOMError.SEVERITY_ERROR, "cdata-section-not-splitted", fCurrentNode);
                        if (!fDOMErrorHandler.handleError(fDOMError))
                        {
                            throw new LSException(LSException.SERIALIZE_ERR, msg);
                        }
                    }
                    else
                    {
                        // issue warning
                        String msg = DOMMessageFormatter.formatMessage(DOMMessageFormatter.SERIALIZER_DOMAIN,
                                "SplittingCDATA", null);
                        modifyDOMError(msg, DOMError.SEVERITY_WARNING, null, fCurrentNode);
                        fDOMErrorHandler.handleError(fDOMError);
                    }
                }
                // split CDATA section
                _printer.printText("]]]]><![CDATA[>");
                index += 2;
                continue;
            }

            if (!XMLChar.isValid(ch))
            {
                // check if it is surrogate
                if (++index < length)
                {
                    surrogates(ch, text.charAt(index), true);
                }
                else
                {
                    fatalError("The character '" + ch + "' is an invalid XML character");
                }
                continue;
            }
            if ((ch >= ' ' && _encodingInfo.isPrintable(ch) && ch != 0x7F) || ch == '\n' || ch == '\r' || ch == '\t')
            {
                _printer.printText(ch);
            }
            else
            {

                // The character is not printable -- split CDATA section
                _printer.printText("]]>&#x");
                _printer.printText(Integer.toHexString(ch));
                _printer.printText(";<![CDATA[");
            }
        }

    }

    protected void myPrintHex(int ch) throws IOException
    {
        _printer.printText("&#x");
        _printer.printText(Integer.toHexString(ch));
        _printer.printText(';');
    }

}
