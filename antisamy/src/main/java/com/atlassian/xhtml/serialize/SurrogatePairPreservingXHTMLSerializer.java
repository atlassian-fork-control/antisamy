package com.atlassian.xhtml.serialize;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

import org.apache.xerces.util.XMLChar;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XHTMLSerializer;

/**
 * <p>
 * Another case of copy-and-pasting from Xerces code to work around a behaviour we don't want.
 * The Xerces serializer will split CDATA sections which contain surrogate pair characters and
 * convert the surrogate to an entity. This is to ensure the output is readable, however our 
 * intended output target is a database so we want to preserve the character instead of converting
 * it to a numeric entity. 
 * </p>
 * <p>
 * See CONFDEV-5428.
 * </p>
 */
public class SurrogatePairPreservingXHTMLSerializer extends XHTMLSerializer
{
    public SurrogatePairPreservingXHTMLSerializer()
    {
        super();
    }

    public SurrogatePairPreservingXHTMLSerializer(OutputFormat format)
    {
        super(format);
    }

    public SurrogatePairPreservingXHTMLSerializer(OutputStream output, OutputFormat format)
    {
        super(output, format);
    }

    public SurrogatePairPreservingXHTMLSerializer(Writer writer, OutputFormat format)
    {
        super(writer, format);
    }

    protected void surrogates(int high, int low, boolean inContent) throws IOException{
        if (XMLChar.isHighSurrogate(high)) {
            if (!XMLChar.isLowSurrogate(low)) {
                //Invalid XML
                fatalError("The character '"+(char)low+"' is an invalid XML character"); 
            }
            else {
                int supplemental = XMLChar.supplemental((char)high, (char)low);
                if (!XMLChar.isValid(supplemental)) {
                    //Invalid XML
                    fatalError("The character '"+(char)supplemental+"' is an invalid XML character"); 
                }
                else {
                    char[] surrogatePair = new char[] { (char)high, (char)low };
                    _printer.printText(surrogatePair, 0, 2);
                }
            }
        } else {
            fatalError("The character '"+(char)high+"' is an invalid XML character"); 
        }
    }
}