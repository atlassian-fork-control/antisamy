package com.atlassian.xhtml.parsing;

import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XMLAttributes;
import org.apache.xerces.xni.XNIException;
import org.cyberneko.html.HTMLElements;
import org.cyberneko.html.HTMLElements.Element;
import org.cyberneko.html.HTMLTagBalancer;

/**
 * An extension to the HTMLTagBalancer which will preserve the self closing syntax/style for a 
 * self closing container element. e.g. a {@code <p/>} should remain a {@code <p/>}. The base class implementation
 * would convert it to {@code <p></p>}.
 */
public class SelfClosingTagPreservingHTMLTagBalancer extends HTMLTagBalancer
{
    @Override
    public void emptyElement(QName element, XMLAttributes attrs, Augmentations augs) throws XNIException
    {
		fDocumentHandler.emptyElement(element, attrs, augs);
    }
}
