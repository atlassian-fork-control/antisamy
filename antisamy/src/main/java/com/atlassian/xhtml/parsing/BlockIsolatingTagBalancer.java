package com.atlassian.xhtml.parsing;

import java.util.HashSet;
import java.util.Set;

import org.apache.xerces.xni.Augmentations;
import org.apache.xerces.xni.QName;
import org.apache.xerces.xni.XMLAttributes;
import org.apache.xerces.xni.XMLResourceIdentifier;
import org.apache.xerces.xni.XMLString;
import org.apache.xerces.xni.XNIException;

import com.atlassian.xhtml.parsing.SelfClosingTagPreservingHTMLTagBalancer;

/**
 * <p>
 * An HTMLTagBalancer that can be configured to treat the body of certain blocks as independently balanced 
 * fragments.
 * </p>
 * <p>
 * This is used in Confluence to deal with CONFDEV-893. Rich text bodies such as those used in links or macros
 * may contain tags that shouldn't be nested in the context of that body. We don't want Neko to perform
 * balancing in these scenarios since it doesn't understand that these rich text bodies should be treat as 
 * distinct from their containing elements.
 * </p>
 */
public class BlockIsolatingTagBalancer extends SelfClosingTagPreservingHTMLTagBalancer implements IsolatedBodyListener
{
    /**
     * The Set of elements identified by rawname that when encountered, should have their body isolated from the owning document.
     */
    private Set<String> elementsToIsolate = new HashSet<String>(2);
    
    /**
     * Set when this balancer is delegating to another (because an isolated body is being handled).
     */
    private BlockIsolatingTagBalancer delegateBalancer;
    
    /** 
     * If this RichTextBodyIsolatingTagBalancer is being used as a delegate by another then
     * delegatingForElementName indicates the name of the tag that caused it to be created.
     * A matching end element should mean this delegate is finished.
     */
    private String delegatingForRawName;
    
    /**
     * The listener is informed when this balancer is finished (it encounters a delegatingForElementName element).
     */
    private IsolatedBodyListener listener;
    
    /**
     * 
     * @param elementRawNamesToIsolate a Set of Element rawnames which when encountered should
     * have their bodies balanced in isolation from their owning structure.
     */
    public BlockIsolatingTagBalancer(Set<String> elementRawNamesToIsolate)
    {
        super();
        this.elementsToIsolate = elementRawNamesToIsolate;
    }

    @Override
    public void startElement(QName elem, XMLAttributes attrs, Augmentations augs) throws XNIException
    {
        if (delegateBalancer != null)
        {
            delegateBalancer.startElement(elem, attrs, augs);
        }
        else if (elementsToIsolate.contains(elem.rawname))
        {
            // subsequent events will all be handled by the delegate
            delegateBalancer = constructNew();
            delegateBalancer.setDelegatingForRawName(elem.rawname);

            // the "trigger" element itself should be consumed by the current tag balancer
            super.startElement(elem, attrs, augs);
        }
        else
        {
            super.startElement(elem, attrs, augs);
        }        
    }

    @Override
    public void endElement(QName element, Augmentations augs) throws XNIException
    {
        if (delegateBalancer != null)
        {
            delegateBalancer.endElement(element, augs);
        }
        else if (delegatingForRawName != null && element.rawname.equals(delegatingForRawName)){
            listener.completeForEndElement(element, augs); // remove this delegate and write the end element
            listener = null;
        }
        else
        {
            super.endElement(element, augs);
        }
    }
    
    @Override
    public void xmlDecl(String version, String encoding, String standalone, Augmentations augs) throws XNIException
    {
        if (delegateBalancer != null)
            delegateBalancer.xmlDecl(version, encoding, standalone, augs);
        else
            super.xmlDecl(version, encoding, standalone, augs);
    }

    @Override
    public void comment(XMLString text, Augmentations augs) throws XNIException
    {
        if (delegateBalancer != null)
            delegateBalancer.comment(text, augs);
        else
            super.comment(text, augs);
    }

    @Override
    public void emptyElement(QName element, XMLAttributes attrs, Augmentations augs) throws XNIException
    {
        if (delegateBalancer != null)
            delegateBalancer.emptyElement(element, attrs, augs);
        else
            super.emptyElement(element, attrs, augs);
    }

    @Override
    public void startGeneralEntity(String name, XMLResourceIdentifier id, String encoding, Augmentations augs)
            throws XNIException
    {
        if (delegateBalancer != null)
            delegateBalancer.startGeneralEntity(name, id, encoding, augs);
        else
            super.startGeneralEntity(name, id, encoding, augs);
    }

    @Override
    public void textDecl(String version, String encoding, Augmentations augs) throws XNIException
    {
        if (delegateBalancer != null)
            delegateBalancer.textDecl(version, encoding, augs);
        else
            super.textDecl(version, encoding, augs);
    }

    @Override
    public void endGeneralEntity(String name, Augmentations augs) throws XNIException
    {
        if (delegateBalancer != null)
            delegateBalancer.endGeneralEntity(name, augs);        
        else
            super.endGeneralEntity(name, augs);
    }

    @Override
    public void endCDATA(Augmentations augs) throws XNIException
    {
        if (delegateBalancer != null)
            delegateBalancer.endCDATA(augs);
        else
            super.endCDATA(augs);
    }

    @Override
    public void characters(XMLString text, Augmentations augs) throws XNIException
    {
        if (delegateBalancer != null)
            delegateBalancer.characters(text, augs);
        else
            super.characters(text, augs);
    }

    @Override
    public void processingInstruction(String target, XMLString data, Augmentations augs) throws XNIException
    {
        if (delegateBalancer != null)
            delegateBalancer.processingInstruction(target, data, augs);
        else
            super.processingInstruction(target, data, augs);
    }

    @Override
    public void ignorableWhitespace(XMLString text, Augmentations augs) throws XNIException
    {
        if (delegateBalancer != null)
            delegateBalancer.ignorableWhitespace(text, augs);
        else
            super.ignorableWhitespace(text, augs);
    }

    public void setDelegatingForRawName(String delegatingForRawName)
    {
        this.delegatingForRawName = delegatingForRawName;
    }

    public void setListener(IsolatedBodyListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void completeForEndElement(QName element, Augmentations augs) throws XNIException
    {
        delegateBalancer = null;
        super.endElement(element, augs);
    }

    /**
     * Construct and return a new {@link BlockIsolatingTagBalancer} which is configured
     * the same as the current {@link BlockIsolatingTagBalancer}.
     * 
     * @return
     */
    protected BlockIsolatingTagBalancer constructNew()
    {
        BlockIsolatingTagBalancer isolatedTagBalancer = new BlockIsolatingTagBalancer(elementsToIsolate);
        
        isolatedTagBalancer.setDocumentHandler(getDocumentHandler());
        isolatedTagBalancer.setDocumentSource(getDocumentSource());
        isolatedTagBalancer.fErrorReporter = fErrorReporter;
        isolatedTagBalancer.tagBalancingListener = tagBalancingListener;
        
        // configure the new HTMLTagBalancer
        isolatedTagBalancer.fNamespaces = fNamespaces;
        isolatedTagBalancer.fAugmentations = fAugmentations;
        isolatedTagBalancer.fReportErrors = fReportErrors;
        isolatedTagBalancer.fDocumentFragment = fDocumentFragment;
        isolatedTagBalancer.fIgnoreOutsideContent = fIgnoreOutsideContent;

        // get properties
        isolatedTagBalancer.fNamesElems = fNamesElems;
        isolatedTagBalancer.fNamesAttrs = fNamesAttrs;
        isolatedTagBalancer.setListener(this);
        
        return isolatedTagBalancer;
    }
}